const cases = setDescriptionTable()

function setDescriptionTable() {
    return {
        validVariableName: "(*=))[a-z,A-Z](*?)<[0-9]>",
        validDeclaredType: "(??)<{typeString}}><{typeNumber}><{validVariableName}>",
        typeString: "\"(*?).\"",
        typeNumber: "(*)[0-9]",
        separator: "(*?) ;(*?) ",
        declare: "var(*=) (=){validVariableName}(*) (?)<=(*) (=){validDeclaredType}>",
        assign: "(=){validVariableName} = (=){validDeclaredType}"
    }
}

function generateMnemonicsTable() {
    
}
